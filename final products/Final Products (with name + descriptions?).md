# 🟨 Final Products (with name + descriptions?)



### Lines

🟨 Hello World :: Yellow Day T-shirt

[Hello World :: Yellow Day T-shirt](https://www.notion.so/Hello-World-Yellow-Day-T-shirt-95cfd09076f7429a88e3ccdc7f0cab58?pvs=21)

🟨 YELLOW DAY! :: Yellow Day T-shir

🟨 YELLOW :: Yellow Day T-shirt

🟨 Have a YELLOW DAY :: Yellow Day T-shirt

~~🟨 YEL-LOW :: Yellow Day T-shirt~~

🟨 It's my B day! B is for Bright :: Yellow Day T-shirt

🟨 Live Love Live Yellow :: Yellow Day T-shirt

🟨 feeling bright today :: Yellow Day T-shirt

🟨 when life gives you lemons, you have the choice to make lemonade :: Yellow Day T-shirt

🟨 I love yellow :: Yellow Day T-shirt

🟨 YELLOW DAY (Top Left Logo) :: Yellow Day T-shirt

🟨 STAFF (making everyday a yellow day) :: Yellow Day T-shirt

🟨 I woke up like this :: Yellow Day T-shirt

🟨 Be the yellow you want to see in the world :: Yellow Day T-shirt

🟨 But God :: Yellow Day T-shirt

🟨 God sees You :: Yellow Day T-shirt

🟨 Just trying to be Yellow :: Yellow Day T-shirt

🟨 Small Man, Big God :: Yellow Day T-shirt

🟨 Small Woman, Big God :: Yellow Day T-shirt

🟨 Yellow People make Yellow People :: Yellow Day T-shirt

🟨 You may be the only yellow someone sees in the world :: Yellow Day T-shirt

🟨 If I wanted to sneak around I wouldn’t be wearing yellow :: Yellow Day T-shirt

🟨 Yellow Day Every Day :: Yellow Day T-shirt

🟨 Every Day Yellow Day :: Yellow Day T-shirt

🟨 Inside Outside Yellow Side :: Yellow Day T-shirt

🟨 You might like heaven :: Yellow Day T-shirt

🟨 There's Hope For That :: Yellow Day T-shirt

🟨 Hope Yellow :: Yellow Day T-shirt

🟨 Jesus is my Yellow :: Yellow Day T-shirt

🟨 Today is Tomorrow :: Yellow Day T-shirt

🟨

🟨

🟨


### Words

🟨 YELLOW YELLOW YELLOW :: Yellow Day T-shirt

🟨 GM :: Yellow Day T-shirt

🟨 Blessed Blessed Blessed :: Yellow Day T-shirt

🟨 BRIGHT BRIGHT BRIGHT :: Yellow Day T-shirt

🟨 SMILE SMILE SMILE :: Yellow Day T-shirt

🟨 YES YES YES :: Yellow Day T-shirt

🟨 LOVE LOVE LOVE :: Yellow Day T-shirt

🟨 Joy Joy Joy :: Yellow Day T-shirt

🟨 PEACE PEACE PEACE :: Yellow Day T-shirt

🟨 FAITH FAITH FAITH :: Yellow Day T-shirt

🟨 Patience Patience Patience :: Yellow Day T-shirt

🟨 HOPE HOPE HOPE :: Yellow Day T-shirt

🟨

🟨

### Designs

~~🟨 Smiley Face :: Yellow Day T-shirt~~

~~🟨 Mustard :: Yellow Day T-shirt~~

🟨 

🟨

🟨

🟨

🟨

### Not Really Real Collection

🟨 We need to help each other. :: Yellow Day T-shirt

🟨 Just trying to be real. :: Yellow Day T-shirt

🟨

### 🎄 Christmas

🎄 Yellow Season :: Yellow Day T-shirt

🎄 How many kings :: Yellow Day T-shirt

🎄 Crown :: Yellow Day T-shirt

🎄


---

### Days of the week :: To let my little light shine :: Yellow Day

🟨💡 Monday gave me the gift of love :: Yellow Day T-shirt

🟨💡 Tuesday peace came from above :: Yellow Day T-shirt

🟨💡 Wednesday told me to have more faith :: Yellow Day T-shirt

🟨💡 Thursday gave me a little more grace :: Yellow Day T-shirt

🟨💡 Friday told me to watch and pray :: Yellow Day T-shirt

🟨💡 Saturday told me just what to say :: Yellow Day T-shirt

🟨💡 Sunday gave me the power divine :: Yellow Day T-shirt

🟨💡 “This little light of mine”  Back: “I'm gonna let it shine” :: Yellow Day T-shirt

Back:

To let my little light shine

The SunShining

[Days of the week :: Yellow Day (My little light collection)](https://www.notion.so/Days-of-the-week-Yellow-Day-My-little-light-collection-ab726a59cb214e628c7dd14a93e8d0a1?pvs=21)


---


### Days of Year :: Calendar Yellow Day

✅🟨📅 Jan :: Yellow Day T-shirt

✅🟨📅 Feb :: Yellow Day T-shirt

✅🟨📅 March :: Yellow Day T-shirt

✅🟨📅 April :: Yellow Day T-shirt

✅🟨📅 May :: Yellow Day T-shirt

✅🟨📅 June :: Yellow Day T-shirt

✅🟨📅 July :: Yellow Day T-shirt

✅🟨📅 Aug :: Yellow Day T-shirt

✅🟨📅 Sept :: Yellow Day T-shirt

✅🟨📅 Oct :: Yellow Day T-shirt

✅🟨📅 Nov :: Yellow Day T-shirt

✅🟨📅 Dec :: Yellow Day T-shirt


---

### 🧔🏻‍♀️ Jesus Collection

🟨🧔🏻‍♀️ Cross :: Yellow Day T-shirt

---


### 💭 Thoughts

🟨💭 THINK BRIGHT THINK HAPPY THINK WARM THINK POSITIVE THINK YELLOW :: Yellow Day T-shirt

🟨💭📖 Yellow Thoughts (Philippians 4:8) :: Yellow Day T-shirt


### 🏕️ RBC

🟨🏕️ Saved by Grace RBC 2023 :: Yellow Day T-shirt


### 🌉 YD - Suicide

🌉 Yellow days are not for jumping :: Yellow Day T-shirt

🌉 Bridge Drawing :: Yellow Day T-shirt

---

### Bible verses

🟨📖

🟨📖

🟨📖

🟨📖

🟨📖

🟨📖

### Song Lyrics

🟨🎶 From the inside out :: Yellow Day T-shirt

🟨🎶 I've got peace like a river. I've got love like an ocean. I've got joy like a fountain. In my soul.

🟨🎶 Christ is my Firm Foundation :: Yellow Day T-shirt

🟨🎶 It's gonna be a good day 'Cause I got a good, good God :: Yellow Day T-shirt

🟨🎶 My REDEEMER LIVES :: Yellow Day T-shirt

🟨🎶 This little light of mine :: Yellow Day T-shirt

🟨🎶

🟨🎶

🟨🎶

🟨🎶

🟨🎶


---


### Hoodies and sweatshirts

🟨 Be the yellow you want to see in the world :: Yellow Day Hoodie

🟨 Cold Days can be Yellow Days :: Yellow Day Hoodie

🟨 Staying Yellow :: Yellow Day Hoodie

🟨 Yellow Season :: Yellow Day Hoodie